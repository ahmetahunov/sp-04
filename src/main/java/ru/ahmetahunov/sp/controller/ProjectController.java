package ru.ahmetahunov.sp.controller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.api.service.IProjectService;
import ru.ahmetahunov.sp.entity.Project;

@Controller
public class ProjectController {

	@Setter
	@NotNull
	@Autowired
	private IProjectService projectService;

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = Exception.class)
	public String errorHandle() {
		return "exception";
	}

	@GetMapping("/project_list")
	public String projectList(@NotNull final Model model) {
		model.addAttribute("projects", projectService.findAll());
		return "project/project_list";
	}

	@GetMapping("/project_info/{id}")
	public String projectInfo(@PathVariable("id") @NotNull final String id, @NotNull final Model model) {
		model.addAttribute("project", projectService.findOne(id));
		return "project/project_info";
	}

	@GetMapping("/project_create")
	public String projectCreate() {
		return "project/project_create";
	}

	@PostMapping("/project_create")
	public String projectCreate(@NotNull final Project project) throws InterruptedOperationException {
		if (project.getName().trim().isEmpty()) throw new InterruptedOperationException();
		projectService.persist(project);
		return "redirect:/project_list";
	}

	@GetMapping("/project_update/{id}")
	public String projectUpdate(@PathVariable("id") @NotNull final String id, @NotNull final Model model) {
		model.addAttribute("projectEdit", projectService.findOne(id));
		return "project/project_update";
	}

	@PostMapping("/project_update")
	public String projectUpdate(@NotNull final Project project, @NotNull final Model model) throws InterruptedOperationException {
		@Nullable final Project found = projectService.findOne(project.getId());
		if (found == null) throw new InterruptedOperationException();
		if (!project.getName().isEmpty())found.setName(project.getName());
		found.setDescription(project.getDescription());
		found.setStartDate(project.getStartDate());
		found.setFinishDate(project.getFinishDate());
		found.setStatus(project.getStatus());
		model.addAttribute("project", projectService.merge(found));
		return "project/project_info";
	}

	@GetMapping("/project_delete/{id}")
	public String projectDelete(@PathVariable("id") @NotNull final String id) {
		projectService.remove(id);
		return "redirect:/project_list";
	}

}
