package ru.ahmetahunov.sp.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.sp.dto.TaskDTO;
import java.util.List;

@FeignClient("task")
public interface TaskClient {

	static TaskClient client(final String baseUrl) {
		final FormHttpMessageConverter converter = new FormHttpMessageConverter();
		final HttpMessageConverters converters = new HttpMessageConverters(converter);
		final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
		return Feign.builder()
				.contract(new SpringMvcContract())
				.encoder(new SpringEncoder(objectFactory))
				.decoder(new SpringDecoder(objectFactory))
				.target(TaskClient.class, baseUrl);
	}

	@GetMapping(value = "/tasks", consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<TaskDTO> getTasks();

	@GetMapping(value = "/tasks", consumes = MediaType.APPLICATION_XML_VALUE)
	public List<TaskDTO> getTasksXML();

	@GetMapping(value = "/tasks/project/{projectId}")
	public List<TaskDTO> getTasksByProjectId(@PathVariable("projectId") @NotNull final String projectId);

	@GetMapping(value = "/tasks/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public TaskDTO getTask(@PathVariable("id") @NotNull final String id);

	@GetMapping(value = "/tasks/{id}",consumes = MediaType.APPLICATION_XML_VALUE)
	public TaskDTO getTaskXML(@PathVariable("id") @NotNull final String id);

	@PostMapping(
			value = "/tasks",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public TaskDTO addTask(@RequestBody @Nullable final TaskDTO taskDTO);

	@PostMapping(
			value = "/tasks",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public TaskDTO addTaskXML(@RequestBody @Nullable final TaskDTO taskDTO);

	@PutMapping(
			value = "/tasks",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public TaskDTO updateTask(@RequestBody @Nullable final TaskDTO taskDTO);

	@PutMapping(
			value = "/tasks",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public TaskDTO updateTaskXML(@RequestBody @Nullable final TaskDTO taskDTO);

	@DeleteMapping("/tasks/{id}")
	public void removeTask(@PathVariable("id") @NotNull final String id);

}
