package ru.ahmetahunov.sp.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.sp.dto.ProjectDTO;
import java.util.List;

@FeignClient
public interface ProjectClient {

	static ProjectClient client(final String baseUrl) {
		final FormHttpMessageConverter converter = new FormHttpMessageConverter();
		final HttpMessageConverters converters = new HttpMessageConverters(converter);
		final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
		return Feign.builder()
				.contract(new SpringMvcContract())
				.encoder(new SpringEncoder(objectFactory))
				.decoder(new SpringDecoder(objectFactory))
				.target(ProjectClient.class, baseUrl);
	}

	@GetMapping(value = "/projects", consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<ProjectDTO> getProjects();

	@GetMapping(value = "/projects", consumes = MediaType.APPLICATION_XML_VALUE)
	public List<ProjectDTO> getProjectsXML();

	@GetMapping(value = "/projects/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ProjectDTO getProject(@PathVariable("id") String id);

	@GetMapping(value = "/projects/{id}", consumes = MediaType.APPLICATION_XML_VALUE)
	public ProjectDTO getProjectXML(@PathVariable("id") String id);

	@PostMapping(
			value = "/projects",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public ProjectDTO addProject(@RequestBody ProjectDTO projectDTO);

	@PostMapping(
			value = "/projects",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public ProjectDTO addProjectXML(@RequestBody ProjectDTO projectDTO);

	@PutMapping(
			value = "/projects",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public ProjectDTO updateProject(@RequestBody ProjectDTO projectDTO);

	@PutMapping(
			value = "/projects",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public ProjectDTO updateProjectXML(@RequestBody ProjectDTO projectDTO);

	@DeleteMapping("/projects/{id}")
	public void removeProject(@PathVariable("id") String id);

}
