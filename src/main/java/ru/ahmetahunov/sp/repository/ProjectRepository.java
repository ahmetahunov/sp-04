package ru.ahmetahunov.sp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ahmetahunov.sp.entity.Project;

public interface ProjectRepository extends JpaRepository<Project, String> {

}
