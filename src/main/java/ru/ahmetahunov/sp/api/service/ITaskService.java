package ru.ahmetahunov.sp.api.service;

import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.sp.entity.Task;
import java.util.List;

public interface ITaskService extends IAbstractService<Task> {

    @NotNull
    public List<Task> findAll(String projectId);

}
