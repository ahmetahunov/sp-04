package ru.ahmetahunov.sp.api.service;

import ru.ahmetahunov.sp.entity.Project;

public interface IProjectService extends IAbstractService<Project> {

}
