package ru.ahmetahunov.sp.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.sp.repository.ProjectRepository;
import ru.ahmetahunov.sp.api.service.IProjectService;
import ru.ahmetahunov.sp.entity.Project;
import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {

    @Setter
    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    public Project persist(@Nullable final Project project) {
        if (project == null) return null;
        if (project.getName().isEmpty()) return null;
        return repository.save(project);
    }

    @Override
    public Project merge(@Nullable final Project project) {
        if (project == null) return null;
        if (project.getName().isEmpty()) return null;
        return repository.save(project);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id).orElse(null);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

}
