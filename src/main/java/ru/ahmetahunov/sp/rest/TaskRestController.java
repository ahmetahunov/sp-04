package ru.ahmetahunov.sp.rest;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.sp.api.service.IProjectService;
import ru.ahmetahunov.sp.api.service.ITaskService;
import ru.ahmetahunov.sp.dto.TaskDTO;
import ru.ahmetahunov.sp.entity.Task;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TaskRestController {

	@Setter
	@NotNull
	@Autowired
	private IProjectService projectService;

	@Setter
	@NotNull
	@Autowired
	private ITaskService taskService;

	@GetMapping(
			value = "/tasks/{id}",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_JSON_VALUE
			})
	public TaskDTO getTask(@PathVariable("id") @NotNull final String id) {
		@Nullable final Task task = taskService.findOne(id);
		if (task == null) return null;
		return task.transformToDTO();
	}

	@GetMapping(
			value = "/tasks",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public List<TaskDTO> getTasks() {
		return taskService.findAll()
				.stream()
				.map(Task::transformToDTO)
				.collect(Collectors.toList());
	}

	@GetMapping(
			value = "/tasks/project/{projectId}",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public List<TaskDTO> getTasksByProject(@PathVariable("projectId") @NotNull final String projectId) {
		return taskService.findAll(projectId)
				.stream()
				.map(Task::transformToDTO)
				.collect(Collectors.toList());
	}

	@PostMapping(
			value = "/tasks",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	@ResponseStatus(HttpStatus.CREATED)
	public TaskDTO addTask(@RequestBody @Nullable final TaskDTO taskDTO) {
		if (taskDTO == null) return null;
		return taskService.persist(taskDTO.transformToTask(projectService)).transformToDTO();
	}

	@PutMapping(
			value = "/tasks",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public TaskDTO updateTask(@RequestBody @Nullable final TaskDTO taskDTO) {
		if (taskDTO == null) return null;
		return taskService.merge(taskDTO.transformToTask(projectService)).transformToDTO();
	}

	@DeleteMapping("/tasks/{id}")
	public void removeTask(@PathVariable("id") @NotNull final String id) {
		taskService.remove(id);
	}

}
