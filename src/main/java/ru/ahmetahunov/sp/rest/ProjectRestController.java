package ru.ahmetahunov.sp.rest;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.sp.api.service.IProjectService;
import ru.ahmetahunov.sp.api.service.ITaskService;
import ru.ahmetahunov.sp.dto.ProjectDTO;
import ru.ahmetahunov.sp.entity.Project;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProjectRestController {

	@Setter
	@NotNull
	@Autowired
	private IProjectService projectService;

	@Setter
	@NotNull
	@Autowired
	private ITaskService taskService;

	@GetMapping(
			value = "/projects/{id}",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public ProjectDTO getProject(@PathVariable("id") @NotNull final String id) {
		@Nullable final Project project = projectService.findOne(id);
		if (project == null) return null;
		return project.transformToDTO();
	}

	@GetMapping(
			value = "/projects",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public List<ProjectDTO> getProjects() {
		return projectService.findAll()
				.stream()
				.map(Project::transformToDTO)
				.collect(Collectors.toList());
	}

	@PostMapping(
			value = "/projects",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public ProjectDTO addProject(@RequestBody @Nullable final ProjectDTO projectDTO) {
		if (projectDTO == null) return null;
		System.out.println(projectDTO.getId());
		System.out.println(projectDTO.getName());
		@NotNull final Project created = projectService.persist(projectDTO.transformToEntity(taskService));
		return created.transformToDTO();
	}

	@PutMapping(
			value = "/projects",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public ProjectDTO updateProject(@RequestBody @Nullable final ProjectDTO projectDTO) {
		if (projectDTO == null) return null;
		@Nullable final Project project = projectService.merge(projectDTO.transformToEntity(taskService));
		if (project == null) return null;
		return project.transformToDTO();
	}

	@DeleteMapping("/projects/{id}")
	public void removeProject(@PathVariable("id") @NotNull final String id) {
		projectService.remove(id);
	}

}
