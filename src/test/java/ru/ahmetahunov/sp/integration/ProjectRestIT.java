package ru.ahmetahunov.sp.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.ahmetahunov.sp.client.ProjectClient;
import ru.ahmetahunov.sp.dto.ProjectDTO;

public class ProjectRestIT {

	@NotNull
	private final String url = "http://localhost:8080/";

	private final ProjectClient projectClient = ProjectClient.client(url);

	@After
	public void clean() {
		for (@NotNull final ProjectDTO projectDTO : projectClient.getProjects()) {
			projectClient.removeProject(projectDTO.getId());
		}
	}

	@Test
	public void addProjectTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProject(projectDTO);
		Assert.assertNotNull(created);
		Assert.assertEquals(projectDTO.getId(), created.getId());
		Assert.assertEquals(projectDTO.getName(), created.getName());
		projectClient.removeProject(created.getId());
		Assert.assertNull(projectClient.getProject(created.getId()));
	}

	@Test
	public void updateProjectTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProject(projectDTO);
		Assert.assertNotNull(created);
		created.setName("test2");
		created.setDescription("description");
		@Nullable final ProjectDTO updated = projectClient.updateProject(created);
		Assert.assertNotNull(updated);
		Assert.assertEquals(created.getName(), updated.getName());
		Assert.assertEquals(created.getDescription(), updated.getDescription());
		Assert.assertNotNull(projectClient.getProject(created.getId()));
		projectClient.removeProject(created.getId());
		Assert.assertNull(projectClient.getProject(created.getId()));
	}

	@Test
	public void removeProjectTest() {
		@Nullable final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		Assert.assertNotNull(projectClient.addProject(projectDTO));
		projectClient.removeProject(projectDTO.getId());
		Assert.assertNull(projectClient.getProject(projectDTO.getId()));
	}

	@Test
	public void getProjectsTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@NotNull final ProjectDTO projectDTO1 = new ProjectDTO();
		projectDTO1.setName("test1");
		@NotNull final ProjectDTO projectDTO2 = new ProjectDTO();
		projectDTO2.setName("test2");
		projectClient.addProject(projectDTO);
		projectClient.addProject(projectDTO1);
		projectClient.addProject(projectDTO2);
		Assert.assertEquals(3, projectClient.getProjects().size());
		projectClient.removeProject(projectDTO.getId());
		projectClient.removeProject(projectDTO1.getId());
		projectClient.removeProject(projectDTO2.getId());
		Assert.assertEquals(0, projectClient.getProjects().size());
	}

	@Test
	public void addProjectXMLTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProjectXML(projectDTO);
		Assert.assertNotNull(created);
		Assert.assertEquals(projectDTO.getId(), created.getId());
		Assert.assertEquals(projectDTO.getName(), created.getName());
		projectClient.removeProject(created.getId());
		Assert.assertNull(projectClient.getProjectXML(created.getId()));
	}

	@Test
	public void updateProjectXMLTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProjectXML(projectDTO);
		Assert.assertNotNull(created);
		created.setName("test2");
		created.setDescription("description");
		@Nullable final ProjectDTO updated = projectClient.updateProjectXML(created);
		Assert.assertNotNull(updated);
		Assert.assertEquals(created.getName(), updated.getName());
		Assert.assertEquals(created.getDescription(), updated.getDescription());
		Assert.assertNotNull(projectClient.getProjectXML(created.getId()));
		projectClient.removeProject(created.getId());
		Assert.assertNull(projectClient.getProjectXML(created.getId()));
	}

	@Test
	public void getProjectsXMLTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@NotNull final ProjectDTO projectDTO1 = new ProjectDTO();
		projectDTO1.setName("test1");
		@NotNull final ProjectDTO projectDTO2 = new ProjectDTO();
		projectDTO2.setName("test2");
		projectClient.addProjectXML(projectDTO);
		projectClient.addProjectXML(projectDTO1);
		projectClient.addProjectXML(projectDTO2);
		Assert.assertEquals(3, projectClient.getProjectsXML().size());
		projectClient.removeProject(projectDTO.getId());
		projectClient.removeProject(projectDTO1.getId());
		projectClient.removeProject(projectDTO2.getId());
		Assert.assertEquals(0, projectClient.getProjectsXML().size());
	}


}
