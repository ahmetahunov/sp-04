package ru.ahmetahunov.sp.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.ahmetahunov.sp.client.ProjectClient;
import ru.ahmetahunov.sp.client.TaskClient;
import ru.ahmetahunov.sp.dto.ProjectDTO;
import ru.ahmetahunov.sp.dto.TaskDTO;

public class TaskRestIT {

	@NotNull
	private final TaskClient taskClient = TaskClient.client(url);

	@NotNull
	private static final String url = "http://localhost:8080/";

	private static ProjectClient projectClient;

	private static ProjectDTO project;

	@BeforeClass
	public static void initProject() {
		projectClient = ProjectClient.client(url);
		project = new ProjectDTO();
		project.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProject(project);
		Assert.assertNotNull(created);
		Assert.assertEquals(project.getId(), created.getId());
	}

	@AfterClass
	public static void removeProject() {
		projectClient.removeProject(project.getId());
		Assert.assertNull(projectClient.getProject(project.getId()));
	}

	@After
	public void clean() {
		for (@NotNull final TaskDTO taskDTO : taskClient.getTasks()) {
			taskClient.removeTask(taskDTO.getId());
		}
	}

	@Test
	public void addTaskTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@Nullable final TaskDTO created = taskClient.addTask(taskDTO);
		Assert.assertNotNull(created);
		Assert.assertEquals(taskDTO.getId(), created.getId());
		Assert.assertEquals(taskDTO.getName(), created.getName());
		taskClient.removeTask(created.getId());
		Assert.assertNull(taskClient.getTask(created.getId()));
	}

	@Test
	public void updateTaskTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@Nullable final TaskDTO created = taskClient.addTask(taskDTO);
		Assert.assertNotNull(created);
		created.setName("test2");
		created.setDescription("description");
		@Nullable final TaskDTO updated = taskClient.updateTask(created);
		Assert.assertNotNull(updated);
		Assert.assertEquals(created.getName(), updated.getName());
		Assert.assertEquals(created.getDescription(), updated.getDescription());
		Assert.assertNotNull(taskClient.getTask(created.getId()));
		taskClient.removeTask(created.getId());
		Assert.assertNull(taskClient.getTask(created.getId()));
	}

	@Test
	public void removeTaskTest() {
		@Nullable final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		Assert.assertNotNull(taskClient.addTask(taskDTO));
		taskClient.removeTask(taskDTO.getId());
		Assert.assertNull(taskClient.getTask(taskDTO.getId()));
	}

	@Test
	public void getTasksTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@NotNull final TaskDTO taskDTO1 = new TaskDTO();
		taskDTO1.setProjectId(project.getId());
		taskDTO1.setName("test1");
		@NotNull final TaskDTO taskDTO2 = new TaskDTO();
		taskDTO2.setProjectId(project.getId());
		taskDTO2.setName("test2");
		taskClient.addTask(taskDTO);
		taskClient.addTask(taskDTO1);
		taskClient.addTask(taskDTO2);
		Assert.assertEquals(3, taskClient.getTasks().size());
		taskClient.removeTask(taskDTO.getId());
		taskClient.removeTask(taskDTO1.getId());
		taskClient.removeTask(taskDTO2.getId());
		Assert.assertEquals(0, taskClient.getTasks().size());
	}

	@Test
	public void getTasksByProjectIdTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@NotNull final TaskDTO taskDTO1 = new TaskDTO();
		taskDTO1.setProjectId(project.getId());
		taskDTO1.setName("test1");
		taskClient.addTask(taskDTO);
		taskClient.addTask(taskDTO1);
		Assert.assertEquals(2, taskClient.getTasksByProjectId(project.getId()).size());
		taskClient.removeTask(taskDTO.getId());
		taskClient.removeTask(taskDTO1.getId());
		Assert.assertEquals(0, taskClient.getTasksByProjectId(project.getId()).size());
	}

	@Test
	public void addTaskXMLTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@Nullable final TaskDTO created = taskClient.addTaskXML(taskDTO);
		Assert.assertNotNull(created);
		Assert.assertEquals(taskDTO.getId(), created.getId());
		Assert.assertEquals(taskDTO.getName(), created.getName());
		taskClient.removeTask(created.getId());
		Assert.assertNull(taskClient.getTaskXML(created.getId()));
	}

	@Test
	public void updateTaskXMLTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@Nullable final TaskDTO created = taskClient.addTaskXML(taskDTO);
		Assert.assertNotNull(created);
		created.setName("test2");
		created.setDescription("description");
		@Nullable final TaskDTO updated = taskClient.updateTaskXML(created);
		Assert.assertNotNull(updated);
		Assert.assertEquals(created.getName(), updated.getName());
		Assert.assertEquals(created.getDescription(), updated.getDescription());
		Assert.assertNotNull(taskClient.getTaskXML(created.getId()));
		taskClient.removeTask(created.getId());
		Assert.assertNull(taskClient.getTaskXML(created.getId()));
	}

	@Test
	public void getTasksXMLTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@NotNull final TaskDTO taskDTO1 = new TaskDTO();
		taskDTO1.setProjectId(project.getId());
		taskDTO1.setName("test1");
		@NotNull final TaskDTO taskDTO2 = new TaskDTO();
		taskDTO2.setProjectId(project.getId());
		taskDTO2.setName("test2");
		taskClient.addTaskXML(taskDTO);
		taskClient.addTaskXML(taskDTO1);
		taskClient.addTaskXML(taskDTO2);
		Assert.assertEquals(3, taskClient.getTasksXML().size());
		taskClient.removeTask(taskDTO.getId());
		taskClient.removeTask(taskDTO1.getId());
		taskClient.removeTask(taskDTO2.getId());
		Assert.assertEquals(0, taskClient.getTasksXML().size());
	}
	
}
